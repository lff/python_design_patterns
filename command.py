#!/usr/bin/env python
'''
command design pattern

use cases:
 o when one needs to keep the history of requests
 o when one needs a callback functionality
 o when requests need to be handled at variant times or orders
 o decouple the invoker from the object handling invocation
 o when an undo functionality is required

Advantages:
 o allows to create a structured in the set of operations such as request, execution which are independent
 o allows to add a new command without having to change existing code (?)

Terminology:
 o command: interface for executing operation
 o concrete command: implementation of an actual command
 o client: creates the concrete command and associates it with a receiver
 o invoker: class that asks the command to carry out the request
 o receiver: class that know how to perform the operation

'''

import abc
import os
import logging as log

history = []

class Command(object):
    ''' command interface '''
    __metaclass__ = abc.ABCMeta


    @abc.abstractmethod
    def execute(self):
        ''' execute the command '''
        raise NotImplementedError('execute method not implemented')


    @abc.abstractmethod
    def undo(self):
        ''' undo previous command '''
        raise NotImplementedError('undo method not implemented')

class LsCommand(Command):
    ''' implementation of the ls command '''

    def __init__(self, receiver):
        self.receiver = receiver

    def execute(self):
        '''command delegates execute call to receiver '''
        self.receiver.show_dir()

    def undo(self):
        ''' ls command does not need undo '''
        pass

class LsReceiver(object):

    def __init__(self, dir):
        self.dir = dir

    def show_dir(self):
        ''' the receiver knows how to execute the command '''

        filenames = [ filename  for filename in os.listdir(self.dir) if os.path.isfile(os.path.join(self.dir, filename))]

        log.info('files in current directory: {}'.format(filenames))
        print('files in current directory: {}'.format(filenames))


class TouchCommand(Command):
    ''' implementeation of touch command in unix '''

    def __init__(self, receiver):
        self.receiver = receiver

    def execute(self):
        self.receiver.create_file()

    def undo(self):
        self.receiver.delete_file()

class TouchReceiver(object):

    def __init__(self, filename):
        self.filename = filename

    def create_file(self):
        ''' actual implementation of the touch command that is going to be executed '''

        with open(self.filename, 'a'):
            os.utime(self.filename, None)

    def delete_file(self):
        ''' actual implementeation of the undo of touch command that is going to be executed '''
        os.remove(self.filename)


class RmCommand(Command):
    ''' command that emulates rm unix command behaviour '''

    def __init__(self, receiver):
        self.receiver = receiver

    def execute(self):
        ''' implementation of remove command '''
        self.receiver.delete_file()

    def undo(self):
        ''' implementation of undo for remove command '''
        self.receiver.undo()

class RmReceiver():

    def __init__(self, filename):
        self.filename = filename
        self.backup_name = None

    def delete_file(self):
        '''actual implementeation of the rm command that is going to be executed '''

        self.backup_name = os.path.join(os.path.dirname(self.filename), '.bkp.' + os.path.basename(self.filename))

        os.rename(self.filename, self.backup_name)

    def undo(self):
        '''actual implementation of the undo of rm command that is going to be implemented '''

        # could perhaps use self.filename
        original_name = os.path.join(os.path.dirname(self.backup_name), os.path.basename(self.backup_name)[5:])

        os.rename(self.backup_name, original_name)

        self.backup_name = None

class Invoker(object):

    def __init__(self, create_file_commands, delete_file_commands):
        self.create_file_commands = create_file_commands
        self.delete_file_commands= delete_file_commands
        self.history = []

    def create_file(self):
        ''' create file method'''

        log.info('Creating file...')

        for command in self.create_file_commands:
            command.execute()
            self.history.append(command)

    def delete_file(self):
        ''' delete file method '''
        log.info('Deleting file...')

        for command in self.delete_file_commands:
            command.execute()
            self.history.append(command)

    def undo_all(self):
        ''' method to undo all changes '''
        log.info('Undoing file changes...')

        [command.undo()  for command in reversed(self.history)]

        log.info('Finished undoing file changes')



log.basicConfig(level=log.INFO)

