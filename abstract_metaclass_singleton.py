# -*- coding: utf-8 -*-
from __future__ import unicode_literals

"""
Created on 07/08/2018

@author: Luis F. de Figueiredo
"""

import logging
import abc
from enum import Enum

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class MetaclassSingleton(abc.ABCMeta):
    '''
    singleton to be used as abstract class

    '''

    _instances = {}

    def __call__(cls, input_1, input_2, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(MetaclassSingleton, cls).__call__(*args, **kwargs)
            cls._instances[cls].input_1 = input_1
            cls._instances[cls].input_2 = input_2
        return cls._instances[cls]


class AbstractSingleton(metaclass=MetaclassSingleton):

    @abc.abstractmethod
    def execute(self):
        raise NotImplemented()


class SingletonOneImplementation(AbstractSingleton):

    def execute(self):
        return 10

class SingletonSecondImplementation(AbstractSingleton):

    def execute(self):
        return 12



class AbstractMetaclassSingleton(object):
    '''
    abstract singleton to be used as base class

    '''

    __metaclass__ = abc.ABCMeta

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(AbstractMetaclassSingleton, cls).__new__(cls)
        return cls.instance

    @abc.abstractmethod
    def execute(self):
        raise NotImplemented()

class AMSImplementationOne(AbstractMetaclassSingleton):

    def __init__(self, input_1, input_2, *args, **kwargs):
        self.input_1 = input_1
        self.input_2 = input_2

    def execute(self):
        return self.input_2

class AMSImplementationTwo(AbstractMetaclassSingleton):

    def __init__(self, input_1, *args, **kwargs):
        self.input_1 = input_1

    def execute(self):
        return self.input_1


class SingletonType(Enum):
    TYPE_ONE = 1
    TYPE_TWO = 2

class AbstractTypedSingleton(object):
    '''
    abstract singleton to be used as base class

    '''

    __metaclass__ = abc.ABCMeta

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(AbstractTypedSingleton, cls).__new__(cls)
        return cls.instance

    def __init__(self, input_1, input_2, input_3):
        super(AbstractTypedSingleton, self).__init__()
        self.input_1 = input_1
        self.input_2 = input_2
        self.input_3 = input_3

    def execute(self):
        return self.singleton

class ATSImplementationOne(AbstractTypedSingleton):

    def __init__(self, input_1, input_2, input_3, *args, **kwargs):
        super(ATSImplementationOne, self).__init__(input_1, input_2, input_3)
        self.singleton_type = SingletonType.TYPE_ONE

class ATSImplementationTwo(AbstractTypedSingleton):

    def __init__(self, input_1, input_2, input_3, *args, **kwargs):
        super(ATSImplementationTwo, self).__init__(input_1, input_2, input_3)
        self.singleton_type = SingletonType.TYPE_TWO
