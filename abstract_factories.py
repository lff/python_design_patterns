 #!/usr/bin/env python
'''
abstract factory pattern

Advantages:
 o simplifies the replacement of product families
 o ensure compatibility of products in the product family
 o allows to isolate concrete classes from the client

Abstract Factory vs Factory Method:
 o use factory method when one needs to decouple a client from a particular product
 o use an abstract factory when clients must be decoupled from the product classes

'''

import logging as log
import abc


log.basicConfig(level=log.INFO)

class ReportProcessor(object):
    '''a client'''

    def __init__ (self, factory):
        self.processor = factory.build_processor()


    def process_report(self):
        try :
          return {'brand' : self.processor.report['brand'],
                'number_of_models' :  self.processor.number_of_models()}
        except Exception as e:
            log.exception('failed to process report')
            raise(e)

class AbstractFactory(object):
    ''' abstract factory interface '''
    __metaclass__ = abc.ABCMeta

    def __init__(self, report):
        self.report = report

    @abc.abstractmethod
    def build_processor(self):
        raise NotImplementedError()

class PeugeotFactory(AbstractFactory):

    def build_processor(self):
        #TODO write report validator factories/validator method
        if self.report['version'] > 1:
            return PeugeotReporter002(self.report)
        return DefaultPeugeotReporter(self.report)

class BMWFactory(AbstractFactory):

    def build_processor(self):
        return DefaultBMWReporter(self.report)

class AbstractPeugeotReporter(object):

    __metaclass__ = abc.ABCMeta

    def __init__ (self, report):
        self.report = report

    @abc.abstractmethod
    def number_of_models(self):
        raise NotImplementedError()

class DefaultPeugeotReporter(AbstractPeugeotReporter):

     def number_of_models(self):
         return len(self.report['model'])

class PeugeotReporter002(AbstractPeugeotReporter):

    def number_of_models(self):
        return sum([ 1 if len(i['category']) == 0 else len(i['category']) for i in self.report['model']])

class AbstractBMWReporter(object):

    __metaclass__ = abc.ABCMeta

    def __init__ (self, report):
        self.report = report

    @abc.abstractmethod
    def number_of_models(self):
        raise NotImplementedError()

class DefaultBMWReporter(AbstractBMWReporter):

     def number_of_models(self):
         return len(self.report['model-name'])
