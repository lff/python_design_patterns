#!/usr/bin/env python
'''
Abstract classes

'''

import logging as log
import math
import abc

log.basicConfig(level=log.INFO)

class AbstractMagic(object):

    __metaclass__ = abc.ABCMeta

    def __init__(self, number):
        self.number = number

    @abc.abstractmethod
    def magic(self):
        ''' Generates a magic number from the original number
        This method should be redefined in the runtime'''
        raise NotImplementedError()


class SquaredMagician(AbstractMagic):

    def repeate_magic(self):
        '''repreat number magid'''
        return math.sqrt(self.magic)

class ExponentialMagician(AbstractMagic):

    def magic(self):
        ''' return the exponent of the number'''
        return math.exp(self.number)

    def repeat_matig(self):
        ''' repeat the magic of exponentiation '''
        return math.exp(self.magic())
