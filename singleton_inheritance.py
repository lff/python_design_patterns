# -*- coding: utf-8 -*-
"""
Created on 23/10/2018

.. sectionauthor:: Luis F. de Figueiredo
"""
from __future__ import unicode_literals
import logging
from enum import Enum
import abc

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

class Tribes(Enum):
    """
    enum
    """
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5


class TribeFactory(object):

    def __init__(self):
        pass

    def build_tribe(self, tribe_type, param):

        if tribe_type is Tribes.B:
            return Brutos(param)

        if tribe_type is Tribes.C:
            return Cartagos(param)

        if tribe_type is Tribes.D:
            return Dianists(param)

        if tribe_type is Tribes.E:
            return Eva(param)

        raise ValueError('unknown tribe: {}'.format(tribe_type))
        return


class Adam(object):

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Adam, cls).__new__(cls)
        return cls.instance

    def __init__(self, param, *args, **kwargs):
        self.param = param
        self.test = self.calculate_param()

    def calculate_param(self):
        return 2**self.param

class Eva(Adam):
    '''
    This is a class with that inherits Adam
    '''

    def __init__(self, param, *args, **kwargs):
        super(Eva, self).__init__(param, *args, **kwargs)
        self.tribe_type = Tribes.E


class Adamists(Adam):
    '''
    This is a class with that inherits Adam
    '''
    pass


class Brutos(Adamists):
    '''
    This is a class with that inherits Adam
    '''

    def __init__(self, param, *args, **kwargs):
        super(Brutos, self).__init__(param, *args, **kwargs)
        self.tribe_type = Tribes.B


class Cartagos(Adamists):
    '''
    This is a class with that inherits Adam
    '''

    def __init__(self, param, *args, **kwargs):
        super(Cartagos, self).__init__(param, *args, **kwargs)
        self.tribe_type = Tribes.C


class Dianists(Adamists):
    '''
    This is a class with that inherits Adam
    '''

    def __init__(self, param, *args, **kwargs):
        super(Dianists, self).__init__(param, *args, **kwargs)
        self.tribe_type = Tribes.D


#####################################################################
# Better structure
#####################################################################

class SingletonFactory(object):

    def __init__(self):
        pass

    def build_singleton(self, tribe_type, param):

        if tribe_type is Tribes.B:
            return BSingle(param)

        if tribe_type is Tribes.C:
            return CSingle(param)

        if tribe_type is Tribes.D:
            return DSingle(param)

        raise ValueError('unknown tribe: {}'.format(tribe_type))
        return


class ASingletonType(type):

    def __call__(cls, param, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ASingletonType, cls).__call__(param, *args, **kwargs)
        return cls.instance

    # nasty way to allow deletion of a singleton
    def __delete__(cls, instance):
        del cls.instance


class AParent(object):

    def __init__(self, param, *args, **kwargs):
        self.param = param
        self.test = self.calculate_param()
        self.tribe_type = Tribes.A

    def calculate_param(self):
        return 2**self.param


class DSingle(AParent, metaclass=ASingletonType):
    '''
    This is a class with that inherits ASingleton
    '''

    def __init__(self, param, *args, **kwargs):
        super(DSingle, self).__init__(param, *args, **kwargs)
        self.tribe_type = Tribes.D


class BSingle(AParent, metaclass=ASingletonType):
    '''
    This is a class with that inherits ASingleton
    '''


    def __init__(self, param, *args, **kwargs):
        super(BSingle, self).__init__(param, *args, **kwargs)
        self.tribe_type = Tribes.B


class CSingle(AParent, metaclass=ASingletonType):
    '''
    This is a class with that inherits ASingleton
    '''


    def __init__(self, param, *args, **kwargs):
        super(CSingle, self).__init__(param, *args, **kwargs)
        self.tribe_type = Tribes.C
