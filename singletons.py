#!/usr/bin/env python
'''
Playing around with singletons

Use cases:
 o one needs to control concurrent access to a shared resource
 o if one needs a global point of access to a resource used in different parts of the system
 o if one needs to have only one instance of an object

Typical cases:
 o log class and subclasses
 o printer
 o database connections
 o file manager
 o retrieve and store information on external configuration files
 o read-only singletons storing global states
 
'''

import argparse
import os
import logging as log


log.basicConfig(level=log.INFO)

class ClassicSingleton(object):
    '''
    This is a class with a single instance and which shares state with all instantiations
    '''

    def __new__(cls, *args, **kwargs):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ClassicSingleton, cls).__new__(cls)
        return cls.instance


class SingletonChild(ClassicSingleton):
    '''
    This is a class with that inherits the ClassicSingleton
    '''
    pass


class BorgSingleton(object):
    '''
    This is a class with a single instance and which shares state with all instantiations
    '''

    _shared_state = {}

    def __new__(cls, *args, **kwargs):

        obj = super(BorgSingleton, cls).__new__(cls, *args, **kwargs)
        obj.__dict__ = cls._shared_state
        return obj

class BorgChild(BorgSingleton):
    '''
    This is a class with that inherits the ClassicSingleton
    '''
    pass

class AnotherBorgChild(BorgSingleton):
    _shared_state = {}
