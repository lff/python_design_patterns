#!/usr/bin/env python
'''
Test factory patterns

'''
import pytest
from ..simple_factories import *
import logging as log
import json


log.basicConfig(level=log.INFO)

def test_simple_factory():

    simple_factory = SimpleFactory.build_data_processor('atg')
    assert type(simple_factory) is ATGProcessor, 'type is not correct'

    simple_factory = SimpleFactory.build_data_processor('ccb')
    assert type(simple_factory) is CCBProcessor, 'type is not correct'


    with pytest.raises(RuntimeError) as error:
        SimpleFactory.build_data_processor('UNKONW')
    assert error.match('^Unknown processor type:') , 'exception not raised when calling unkown type of data'
