# -*- coding: utf-8 -*-
"""
Created on 23/10/2018

.. sectionauthor:: Luis F. de Figueiredo <lfdefigueiredo@evonetix.com>
"""
from __future__ import unicode_literals

import pytest
import os
import logging

from ..singleton_inheritance import *


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def test_problematic_structure():

    tribe_b = TribeFactory().build_tribe(Tribes.B, Tribes.B.value)

    assert isinstance(tribe_b, Adamists), 'failed to instantiate singleton from correct parent tribe'

    tribe_c = TribeFactory().build_tribe(Tribes.C, Tribes.C.value)

    assert isinstance(tribe_c, Adamists), 'failed to instantiate another singleton from correct parent tribe'
    assert tribe_c != tribe_b, 'singleton instances are the same'

    tribe_b_again = TribeFactory().build_tribe(Tribes.B, Tribes.B.value)

    assert tribe_b == tribe_b_again, 'created another singleton instance of the same tribe'

    # the problem starts when we instantiate the parent singleton
    tribe_a = Adamists(Tribes.A.value)

    assert tribe_a != tribe_b, 'singletons have the same instance'
    assert tribe_a != tribe_c, 'singletons have the same instance'

    tribe_d = TribeFactory().build_tribe(Tribes.D, Tribes.D.value)

    logger.error('\nthe two classes should have been two different singletons instances but instead but instead '
                 'the new singletons will start using the parents instance whereas already existing instances '
                 'will remain different:\n {} == {} != {} != {}'.format(tribe_a, tribe_d, tribe_b, tribe_c))

    assert tribe_a != tribe_b, 'singletons have the same instance'
    assert tribe_a != tribe_c, 'singletons have the same instance'
    assert tribe_a == tribe_d, 'singletons different instance'

    tribe_b = TribeFactory().build_tribe(Tribes.B, Tribes.B.value)

    logger.error('\ntrying to instantiate an existing child singleton again does not change that instance\n {} != {}'
                 .format(tribe_a, tribe_b))

    assert tribe_a != tribe_b, 'singletons have the same instance'

    tribe_a_again = Adamists(3 * Tribes.A.value)

    logger.error('\nparent instance can be update but remain a singleotn\n {} == {} != {}'
                 .format(tribe_a, tribe_a_again, tribe_b))

    assert tribe_a == tribe_a_again, 'created another singleton instance'
    assert tribe_a_again.param == 3 * Tribes.A.value, 'singletons parameter has not been updated'
    assert tribe_d.param == 3 * Tribes.A.value, 'singletons parameter has not been updated'



def test_correct_structure():

    b_single = SingletonFactory().build_singleton(Tribes.B, Tribes.B.value)
    assert isinstance(b_single, AParent), 'singleton inherited parent class'
    assert b_single.test == 2 ** Tribes.B.value, 'failed to correctly calculate test attribute'

    c_single = SingletonFactory().build_singleton(Tribes.C, Tribes.C.value)
    assert isinstance(c_single, AParent), 'singleton inherited parent class'
    assert c_single != b_single, 'singletons share the same instance'

    new_b_single = SingletonFactory().build_singleton(Tribes.B, Tribes.B.value * 3)

    assert b_single == new_b_single, 'created a new singleton instance'
    assert b_single.test == 2**Tribes.B.value, 'singletons instance test attribute have not been updated'
    assert new_b_single.test == 2**Tribes.B.value, 'singleton instance test attribute have not been updated'

    # we now try to instantiate the parent class
    a_parent = AParent(Tribes.A.value)
    assert isinstance(a_parent, AParent), 'parent class has different instance of itself'

    # instantiate new singletons childs
    d_single = SingletonFactory().build_singleton(Tribes.D, Tribes.D.value)
    b_single = SingletonFactory().build_singleton(Tribes.B, Tribes.B.value)

    a_new_parent = AParent(Tribes.A.value)

    logger.info('\nthe two classes are now two different objects independently if the parent has been instantiated\n'
                '{} != {} != {}'.format(a_parent, d_single, b_single))

    assert a_parent != c_single, 'parent instance is the same as c child singleton'
    assert new_b_single == b_single, 'b singletons are no longer singletons'
    assert a_parent != b_single, 'parent instance is the same as c child singleton'
    assert a_parent != d_single, 'parent instance is the same as d child singleton'
    assert a_parent != a_new_parent, 'instances of parent class are behaving like singletons'

    d_single_again = SingletonFactory().build_singleton(Tribes.D, 2 * Tribes.D.value)

    assert d_single_again == d_single, 'd singletons are no longer singletons'
    assert d_single.tribe_type == Tribes.D, 'singleton instance parameter has been updated'
    assert d_single_again.tribe_type == Tribes.D, 'singleton instance parameter has been updated'
    assert d_single_again.param != 2 * Tribes.D.value, 'parameter has a value different from initial one'
    assert d_single_again.param == Tribes.D.value, 'parameter has a value different from initial one'


def test_singleton_persistence():

    d_single = SingletonFactory().build_singleton(Tribes.D, 2 * Tribes.D.value)
    assert d_single.param == Tribes.D.value, 'parameter has a value different from initial one'

    # trying to delete the object but this will only delete the variable
    del d_single

    assert hasattr(DSingle, 'instance'), 'singleton has no longer instance attribute'
    assert getattr(DSingle, 'instance') is not None, 'singleton instance has been deleted'
    assert 'd_single' not in globals(), 'failed to delete variable from globals'
    assert 'd_single' not in locals(), 'failed to delete variable from locals'

    d_single = getattr(DSingle, 'instance')
    d_single_again = SingletonFactory().build_singleton(Tribes.D, 2 * Tribes.D.value)

    assert d_single == d_single_again, 'there is a new instance of singleton'
    assert d_single_again.param == Tribes.D.value, 'parameter has a value different from initial one'

    # one has to force delete the instance, however there is still an object linked to it d_single, d_single ghost
    DSingle.__delete__(DSingle(None))
    assert not hasattr(DSingle, 'instance'), 'singleton instance has been deleted'
    assert 'd_single' in locals(), 'removed singleton from local variables'

    d_single_again = SingletonFactory().build_singleton(Tribes.D, 2 * Tribes.D.value)
    assert d_single_again.param == 2 * Tribes.D.value, 'parameter has been updated because we have a new instance'
    assert d_single != d_single_again, 'ghost singleton turned into real singleton'

    d_ghost = d_single

    d_single = SingletonFactory().build_singleton(Tribes.D, 3 * Tribes.D.value)
    assert d_single.param == 2 * Tribes.D.value, 'parameter has been updated because we have a new instance'
    assert d_single == d_single_again, 'created new instance of singleton'
    assert d_ghost != d_single, 'exorcised ghost singleton'


