#!/usr/bin/env python
'''
Test proxy patterns

'''
import pytest
from ..proxy import *
import logging as log

log.basicConfig(level=log.INFO)

## danger zone
@pytest.mark.skip(reason="takes to long to run")
def test_proxy():

    proxy1 = Proxy()

    assert proxy1.reference_count == 1, 'did not create a proxy'

    sorted_proxy1 = list(reversed(sorted(proxy1.__class__.cached_object.digits)))

    assert proxy1.__class__.cached_object.digits != sorted_proxy1, 'digits are already sorted'

    proxy2 = Proxy()
    proxy3 = Proxy()

    assert proxy1.reference_count == 3, 'did not create all 3 proxies'

    sorted_proxy3 = sorted(proxy3.__class__.cached_object.digits)
    assert proxy3.__class__.cached_object.digits != sorted_proxy3, 'digits are already sorted'

    proxy1.sort(reverse = True)

    assert proxy1.__class__.cached_object.digits == sorted_proxy1, 'digits are not reverse sorted'

    del proxy2

    assert proxy1.reference_count == 2, 'did not delete one of the proxies'
    assert proxy3.__class__.cached_object.digits != sorted_proxy3, 'digits are already sorted'

    # the object is the same, we are just using the proxies to access and manipulate it
    assert proxy3.__class__.cached_object.digits == sorted_proxy1, 'digits are not reverse sorted'

    proxy3.sort()
    assert proxy3.__class__.cached_object.digits == sorted_proxy3, 'digits are not sorted'

