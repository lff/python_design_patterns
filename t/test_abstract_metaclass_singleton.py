# -*- coding: utf-8 -*-
from __future__ import unicode_literals

"""
Created on 07/08/2018

@author: Luis F. de Figueiredo
"""

import logging
import pytest
from ..abstract_metaclass_singleton import *

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def test_metaclass_singleton ():

    a = SingletonOneImplementation(1, 3)
    b = SingletonOneImplementation(2, 4)

    assert a is b, 'SingletonOneImplementation is not a singleton'
    assert (1, 3) == (b.input_1, b.input_2), 'SingletonOneImplementation changed state'

    a = SingletonOneImplementation(1, 3)
    b = SingletonSecondImplementation(5, 6)

    assert a is not b, 'failed to create a second singleton implementation'
    assert a.execute() != b.execute(), 'different implementations are doing the same thing'
    assert (1, 3) == (a.input_1, a.input_2 ), 'SingletonOneImplementation did not keep the state'
    assert (5, 6) == (b.input_1, b.input_2), 'SingletonSecondImplementation state was not initialized'

    c = SingletonSecondImplementation(7, 8)
    assert c is not a, 'failed to create a second singleton implementation'
    assert c is b, 'failed second singleton implementation is not reusable'
    assert c.execute() == b.execute(), 'failed second singleton implementation is not executing the same value'
    assert (5, 6) == (c.input_1, c.input_2), 'SingletonSecondImplementation changed state'

    del c, b
    d = SingletonSecondImplementation(7, 8)
    assert (5, 6) == (d.input_1, d.input_2), 'SingletonSecondImplementation changed state'


def test_abstract_metaclass_singleton():

    a = AMSImplementationOne(1, 3)
    b = AMSImplementationOne(2, 4)

    assert a is b, 'AMSImplementationOne is not a singleton'

    a = AMSImplementationOne(1, 3)
    b = AMSImplementationTwo(1)

    assert a is not b, 'failed to create AMSImplementationTwo'
    assert a.execute() != b.execute(), 'different implementations are doing the same thing'

    assert (1, 3) == (a.input_1, a.input_2), 'AMSImplementationOne did not keep the state'
    with pytest.raises(AttributeError) as e:
        b.input_2

    assert '\'AMSImplementationTwo\' object has no attribute \'input_2\'' == e.value.args[0],\
        'failed to capture correct exception'

    assert 1 == b.input_1, 'AMSImplementationTwo state was not initialized'


def test_abstract_typed_singleton():

    a = ATSImplementationOne(input_3=4, *(1, 3))
    b = ATSImplementationTwo(input_3=5, *(2, 4))

    assert a != b, 'failed to create borg implementations'
    assert SingletonType.TYPE_ONE == a.singleton_type, 'failed to get correct singleton type for type one'
    assert SingletonType.TYPE_TWO == b.singleton_type, 'failed to get correct singleton type for type two'
    assert (1, 3, 4) == (a.input_1, a.input_2, a.input_3), 'failed to store input values for type one'
    assert (2, 4) == (b.input_1, b.input_2), 'failed to store input values for type two'

