#!/usr/bin/env python
'''
Test factory patterns

'''
import pytest
from ..abstract_factories import *
import logging as log
import json


log.basicConfig(level=log.INFO)

def test_abstract_factory():


    peugeot_report_default = { 'version' : 0.1, 'model' : ['105', '206', '206 - lx'] }

    peugeot_report_002 = { 'version': 2.2, 'brand' : 'peugeot' , 'model': [{'name' : '105', 'category' : [] },
                                                        {'name' : '206', 'category' : ['lx', 'gti'] },
                                                        {'name' : '308', 'category' : ['cc']}
                                                        ]}

    with pytest.raises(KeyError) as error:
        processed_report = ReportProcessor(PeugeotFactory(peugeot_report_default)).process_report()
    error.match('brand'), 'failed to throw '

    peugeot_report_default['brand'] = 'peugeot'

    processed_report = ReportProcessor(PeugeotFactory(peugeot_report_default)).process_report()

    assert processed_report['number_of_models'] == 3, 'incorrect number of models using report version 0.1 for peugeot'

    processed_report = ReportProcessor(PeugeotFactory(peugeot_report_002)).process_report()

    assert processed_report['number_of_models'] == 4, 'incorrect number of models using report version 2.2 for peugeot'

    with pytest.raises(KeyError) as error:
        processed_report = ReportProcessor(BMWFactory(peugeot_report_default)).process_report()
    error.match('model-name'), 'failed to throw KeyError when using a Peugeot report in a BMWFactory'

