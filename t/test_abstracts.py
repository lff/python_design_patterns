#!/usr/bin/env python
'''
++++DESCRIPTION HERE++++

'''
import pytest
from ..abstracts import *
import logging as log


log.basicConfig(level=log.INFO)

def test_abstract_number():

    abstract_magician = AbstractMagic(4)

    with pytest.raises(NotImplementedError) as error:
        abstract_magician.magic()

def test_squared_magic():
    ''' test a class that does not implement the abstract method'''
    squared_magician = SquaredMagician(4)

    with pytest.raises(NotImplementedError) as error:
        squared_magician.magic()

    # repeat_magic method needs a float but magic method is still not implemented
    with pytest.raises(TypeError) as error:
        squared_magician.repeate_magic()

    assert error.match('a float is required') , 'exception not raised when calling implemented repeat_magic method'

def test_exponential_magic():
    '''test a class that implementes the abstract method '''

    exponential_magician = ExponentialMagician(2)

    assert exponential_magician.magic() == math.exp(2), 'magic did not happen'
    assert exponential_magician.repeat_matig() == math.exp(math.exp(2)), 'magic was not repeated'

