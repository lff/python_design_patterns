#!/usr/bin/env python
'''
test observer designs
'''

import pytest
import logging as log
import re
import tempfile
from ..command import *

log.basicConfig(level=log.INFO)


def test_command():

    tmp_dir = tempfile.mkdtemp()
    log.debug('tmp_dir: {}'.format(tmp_dir))

    ls_receiver = LsReceiver(tmp_dir)
    ls_command = LsCommand(ls_receiver)

    touch_receiver = TouchReceiver(os.path.join(tmp_dir, 'test_file'))
    touch_cmd = TouchCommand(touch_receiver)

    rm_receiver = RmReceiver(os.path.join(tmp_dir, 'test_file'))
    rm_cmd = RmCommand(rm_receiver)

    create_file_commands = [ls_command, touch_cmd, ls_command]
    delete_file_commands = [ls_command, rm_cmd, ls_command]

    invoker = Invoker(create_file_commands, delete_file_commands)

    invoker.create_file()
    assert  [ filename  for filename in os.listdir(tmp_dir) if os.path.isfile(os.path.join(tmp_dir, filename))] == ['test_file'] \
            , 'failed to create test file'

    invoker.delete_file()
    assert [filename for filename in os.listdir(tmp_dir) if os.path.isfile(os.path.join(tmp_dir, filename))] == [
        '.bkp.test_file'] \
        , 'failed to delete test file'

    invoker.undo_all()
    assert  [ filename  for filename in os.listdir(tmp_dir) if os.path.isfile(os.path.join(tmp_dir, filename))] == [] \
            , 'failed to undo test file deletion'
