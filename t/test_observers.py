#!/usr/bin/env python
'''
test observer designs
'''

import pytest
import logging as log
import re
from ..observers import *

log.basicConfig(level=log.INFO)


def test_micromanaged_observers():

    subject = MicroManagerSubject()

    assert len(subject.observers) == 0, 'subject has already an observer'

    log.info('Registering USA observer')
    usa_observer = USATimeObsever('bob')
    subject.register_observer(usa_observer)
    assert len(subject.observers) == 1, 'subject did not register observer'
    subject.notify_observers()
    log.info(usa_observer.last_time_note)
    ## very basic check
    assert str(usa_observer.last_time_note).endswith('M'), 'USA observer is not reporting the correct time'


    eu_observer = EUTimeObserver('jane')
    subject.register_observer(eu_observer)
    assert len(subject.observers) == 2, 'subject did not register EU observer'
    assert eu_observer.last_time_note == None, 'EU observer already has a note'
    subject.notify_observers()
    log.info(usa_observer.last_time_note)
    log.info(eu_observer.last_time_note)
    ## very basic check
    assert not str(eu_observer.last_time_note).endswith('M'), 'EU observer is reporting like the USA observer'
    assert str(usa_observer.last_time_note).split('says:')[1]  != str(eu_observer.last_time_note).split('says:')[1], 'both observers are reporing the same way'

    usa_observer2 = USATimeObsever('alice')
    subject.register_observer(usa_observer2)
    assert len(subject.observers) == 3, 'subject did not register third observer'
    subject.notify_observers()
    assert str(usa_observer2.last_time_note).split('says:')[1] == str(usa_observer.last_time_note).split('says:')[1], 'USA observers were not notified with the same current time'

    subject.register_observer(usa_observer)
    assert len(subject.observers) == 3, 'subject registered an observer that is already registered'

    subject.unregister_observer(usa_observer)
    time.sleep(2)
    assert len(subject.observers) == 2, 'subject unregistered observer'
    subject.notify_observers()
    assert str(usa_observer2.last_time_note).split('says:')[1] != str(usa_observer.last_time_note).split('says:')[
        1], 'USA observers have now diferent times because one is no longer registered'


def test_independent_observers():

    subject = DelegatingSubject()

    assert len(subject.observers) == 0, 'subject has already an observer'

    log.info('Registering independent USA observer')
    usa_observer = IndependentUSATimeObsever('independent bob')
    subject.register_observer(usa_observer)
    assert len(subject.observers) == 1, 'subject did not register observer'
    subject.notify_observers()
    log.info(usa_observer.last_time_note)
    ## very basic check
    assert str(usa_observer.last_time_note).endswith('M'), 'USA observer is not reporting the correct time'

    eu_observer = IndependentEUTimeObserver('independent jane')
    subject.register_observer(eu_observer)
    assert len(subject.observers) == 2, 'subject did not register EU observer'
    assert eu_observer.last_time_note == None, 'EU observer already has a note'
    subject.notify_observers()
    log.info(usa_observer.last_time_note)
    log.info(eu_observer.last_time_note)
    ## very basic check
    assert not str(eu_observer.last_time_note).endswith('M'), 'EU observer is reporting like the USA observer'
    assert str(usa_observer.last_time_note).split('says:')[1] != str(eu_observer.last_time_note).split('says:')[
        1], 'both observers are reporing the same way'

