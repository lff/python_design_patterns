#!/usr/bin/env python
'''
Test facade patterns

'''
import pytest
from ..facades import *
import logging as log

log.basicConfig(level=log.INFO)

def test_facades():


    peugeot_report_default = { 'version' : 0.1, 'model' : ['105', '206', '206 - lx'] }

    peugeot_report_002 = { 'version': 2.2, 'brand' : 'peugeot' , 'model': [{'name' : '105', 'category' : [] },
                                                        {'name' : '206', 'category' : ['lx', 'gti'] },
                                                        {'name' : '308', 'category' : ['cc']}
                                                        ]}

    with pytest.raises(KeyError) as error:
        processed_report = ReportProcessorFacade().process_report(peugeot_report_default)
    error.match('brand'), 'failed to throw '

    peugeot_report_default['brand'] = 'peugeot'

    processed_report = ReportProcessorFacade().process_report(peugeot_report_default)

    assert processed_report['number_of_models'] == 3, 'incorrect number of models using report version 0.1 for peugeot'

    processed_report = ReportProcessorFacade().process_report(peugeot_report_002)

    assert processed_report['number_of_models'] == 4, 'incorrect number of models using report version 2.2 for peugeot'

    bmw_report = {'version': 2.2, 'brand': 'BMW', 'model': [ 'z3', 'm350']}

    with pytest.raises(KeyError) as error:
        processed_report = ReportProcessorFacade().process_report(bmw_report)
    error.match('model-name')

    bmw_report['model-name'] = bmw_report['model']
    del bmw_report['model']

    processed_report = ReportProcessorFacade().process_report(bmw_report)
    assert processed_report['number_of_models'] == 2, 'incorrect number of models for bmw report'

    vw_report = {'version': 2.2, 'brand': 'VW', 'model': ['passat', 'polo']}
    with pytest.raises(Exception) as error:
        processed_report = ReportProcessorFacade().process_report(vw_report)
    error.match('Could not find the correct report factory')

