#!/usr/bin/env python
'''
test various scenarios in singleton class

'''

import argparse
import os, re
import logging as log
import pytest
from ..singletons import *

log.basicConfig(level=log.INFO)

def test_classic_singleton():
    ''' test uniqueness of ClassicSingleton '''
    singleton = ClassicSingleton()

    another_singleton = ClassicSingleton()

    assert singleton is another_singleton, 'objects are not the same instance'

    var = 'I am only one var'

    singleton.only_one_var = var

    assert another_singleton.only_one_var == var, 'instance was not updated'

@pytest.mark.skip(reason="Example in the book does not seem to work")
def test_singleton_child():
    ''' Test inheritance of ClassicSingleton'''

    singleton = ClassicSingleton()

    # SingletonChild is a class that inherits ClassicSingleton
    child = SingletonChild()
    var = 'I am only one var'
    singleton.only_one_var = var

    assert child is not singleton, 'child object is a ClassicSingleton instance'

    with pytest.raises(AttributeError) as error:
        child.only_one_var == var

    assert error.match("^'SingletonChild' instance has no attribute 'only_one_var'"), 'the singleton child shares same state as classic singleton'

def test_borg_singleton():
    '''test non-uniquess of BorgSingleton'''

    borg = BorgSingleton()

    another_borg = BorgSingleton()

    assert borg is not another_borg, 'the BorgSingletons are still the same instance'

def test_borg_childs():
    ''' testing the inheritance of the borg child'''

    borg = BorgSingleton()
    borg_child = BorgChild()

    var = 'I am only one var'

    borg.only_one_var = var

    assert borg_child is not borg, 'the borg child is a borg'
    assert borg_child.only_one_var == var, 'child did not inherited the borg state'

    another_borg_child = AnotherBorgChild()

    assert another_borg_child is not borg, 'the other borg child is a borg'

    with pytest.raises(AttributeError) as error:
        another_borg_child.only_one_var == var
    assert error.match("^'AnotherBorgChild' object has no attribute 'only_one_var'"), 'the other borg child still shares the same state'
