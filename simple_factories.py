#!/usr/bin/env python
'''
factory method patterns

Advantages:
 o allows to increase the scope of the code as it is not tied to the concrete classes but to the interfaces
 o decouples code that creates objects from code that uses the objects
 o it separates the interfaces from their implementations

'''

import logging as log
import abc


log.basicConfig(level=log.INFO)

class SimpleFactory(object):

    @staticmethod
    def build_data_processor(type):
        if type == 'atg':
            return ATGProcessor()
        elif type == 'ccb':
            return CCBProcessor()
        else:
            raise RuntimeError('Unknown processor type: {}'.format(type))

class ATGProcessor(object):
    pass

class CCBProcessor():
    pass

