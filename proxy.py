#!/usr/bin/env python
'''
proxy design pattern

Used when one wants to extend the functionality of another object for example:
 o control access to another object for security reasons
 o log all calls to subjects with its parameters
 o connect to subject which is located in remote machine or another address space
 o instantiate heavy object only when it is really needed allowing also to cache a heavy object
 o temporarily store some calculation results
 o count references to an object

Advantages:
 o allows to optimize the performance of an application
 o allows to improve security of an application
 o facilitates interaction between remote systems
 o can be used to increase response time from the object

'''

import logging as log


log.basicConfig(level=log.INFO)


from abc import ABCMeta, abstractmethod
import random

class AbstractSubject(object):
    '''a common interface for the proxy object'''

    __metaclass__ = ABCMeta

    @abstractmethod
    def sort(self, reverse = False):
        raise NotImplementedError()

class RealSubject(AbstractSubject):
    ''' class implementing the abstractSubject which will consume a lot of cpu and memory '''

    def __init__(self):
        ## in python 2 one can use xrange
        self.digits = [ random.random() for i in range(10000000)]

    def sort(self, reverse = False):
        self.digits.sort()

        if reverse:
            self.digits.reverse()

class Proxy(AbstractSubject):
    '''A proxy which has the same interface as RealSubject'''

    reference_count = 0

    def __init__(self):
        '''a constructor which created the object or recover it from cache'''

        if not getattr(self.__class__, 'cached_object', None):
            self.__class__.cached_object = RealSubject()
            log.info('Created new object')
        else:
            log.info('using cached object')

        self.__class__.reference_count += 1

        log.info('Number of references: {}'.format(self.__class__.reference_count))

    def sort(self, reverse = False):
        ''' The arguments are logged by the proxy but the hard lifitng is done by the real class'''

        log.info('Called sort method with arguments: {}'.format(locals().items()))

        # do the heavy lifting
        self.__class__.cached_object.sort(reverse = reverse)

    def __del__(self):
        '''clean upif there are not instances'''

        self.__class__.reference_count -= 1

        if self.__class__.reference_count == 0:
            log.info('Number fo reference counts is 0 so object will be deleted')

            del self.__class__.cached_object

        log.info('Deleted object. Number of objects remaining; {}'.format(self.__class__.reference_count))
