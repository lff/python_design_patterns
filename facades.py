#!/usr/bin/env python
'''
example of a facade Obect

Advantages:
 o allows loose coupling between client and subsystems
 o provides interface to a set of interfaces in the subsystem
 o wraps complicated subsystem with simpler interface
 o allows to have a flexible subsytem and simplify the client interface

'''


import logging as log
from .abstract_factories import *

log.basicConfig(level=log.INFO)

class ReportProcessorFacade(object):
    '''class to process any report'''

    def process_report(self, report):

        try :
            if str(report['brand']).lower() == 'peugeot':
                facade = ReportProcessor(PeugeotFactory(report))
                return {'brand' : facade.processor.report['brand'],
                    'number_of_models' :  facade.processor.number_of_models()}
            elif (report['brand']).lower() == 'bmw' :
                facade = ReportProcessor(BMWFactory(report))
                return {'brand' : facade.processor.report['brand'],
                    'number_of_models' : facade.processor.number_of_models()}
            else:
                raise Exception('Could not find the correct report factory')
        except Exception as e:
            log.exception('failed to process report')
            raise(e)
