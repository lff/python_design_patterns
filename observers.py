#!/usr/bin/env python
"""
observer design pattern

This patterns is used when a change in one object leads to a change in another object
and one doesn't know how many objects should be changed nor how they should be changed

Advantages:
o allows loose coupling between Subject and Observers
o message is broadcasted to observers
o number of observers can be changed at runtime
o Subject can keep any number of observers

"""

import argparse
import os
import logging as log
import time
import datetime
from abc import ABCMeta, abstractmethod

log.basicConfig(level=log.INFO)


class AbstractSubject(object):
    """ an abstract subject """

    def __init__(self):
        self.observers = set()
        self.current_time = None

    def register_observer(self, observer):
        """register observer that needs info about subject"""
        self.observers.add(observer)

    def unregister_observer(self, observer):
        """unregister observer that needs infor about subject"""
        try:
            self.observers.remove(observer)
        except ValueError:
            log.error(ValueError('No such observer in subject'))

    def notify_observers(self):
        """notify observer with the time change"""
        raise NotImplementedError()


class MicroManagerSubject(AbstractSubject):
    """a subject that micromanages the observers"""

    def notify_observers(self):
        """notify observer with the time change"""
        self.current_time = time.time()
        for observer in self.observers:
            observer.notify(self.current_time)


class DelegatingSubject(AbstractSubject):
    """a subject that delegates notification to observers"""

    def notify_observers(self):
        """notify observer about time change by passing itself to the observer"""
        self.current_time = time.time()
        for observer in self.observers:
            observer.notify(self)


class AbstractObserver(object):
    """ abstract observer that provides the different notification methods as interface for subjects """

    __metaclass__ = ABCMeta

    @abstractmethod
    def notify(self, unix_timestamp):
        """ abstract method for notification """
        raise NotImplementedError()


class USATimeObsever(AbstractObserver):
    """ micro-managed observer from USA """

    def __init__(self, name):
        self.name = name
        self.last_time_note = None

    def notify(self, unix_timestamp):
        """ USA implementation of notify """
        self.last_time_note = 'Observer {} says: {}'.format(
            self.name, datetime.datetime.fromtimestamp(int(unix_timestamp)).strftime('%m-%d-%Y %I:%M:%S%p'))


class EUTimeObserver(AbstractObserver):
    """ micro-managed observer from EU """

    def __init__(self, name):
        self.name = name
        self.last_time_note = None

    def notify(self, unix_timestamp):
        """ EU implementation of notify """
        self.last_time_note = 'Observer {} says: {}'.format(
            self.name, datetime.datetime.fromtimestamp(int(unix_timestamp)).strftime('%d-%m-%Y %H:%M:%S'))


class IndependentUSATimeObsever(AbstractObserver):
    """ independent observer from USA """

    def __init__(self, name):
        self.name = name
        self.last_time_note = None

    def notify(self, subject):
        """ USA implementation of notify """
        self.last_time_note = 'Observer {} says: {}'.format(
            self.name, datetime.datetime.fromtimestamp(int(subject.current_time)).strftime('%m-%d-%Y %I:%M:%S%p'))


class IndependentEUTimeObserver(AbstractObserver):
    """ independent observer from EU """

    def __init__(self, name):
        self.name = name
        self.last_time_note = None

    def notify(self, subject):
        """ EU implementation of notify """
        self.last_time_note = 'Observer {} says: {}'.format(
            self.name, datetime.datetime.fromtimestamp(int(subject.current_time)).strftime('%d-%m-%Y %H:%M:%S'))
