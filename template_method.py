#!/usr/bin/env python
'''
template method implmentation

Sequence of steps:
1. create a method for the algorithm
2. split algorithms implementation into different steps
3. common methods are implemented in the base class al specific methods will be re-implemented

Advantages:
 o minimize code duplication
 o centralization of the algorithms location
 o ease code modification and expansion

'''

import abc
import os
import re
import logging as log
import urllib
from xml.dom import minidom

log.basicConfig(level=log.INFO)

class AbstractNewsParser(object):
    ''' abstract news parser '''

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def get_top_new(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def get_url(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def get_raw_content(self, url):
        raise NotImplementedError()

    @abc.abstractmethod
    def parse_content(self, content):
        raise NotImplementedError()

    @abc.abstractmethod
    def get_field_map(self):
        raise NotImplementedError()

    @abc.abstractmethod
    def crop(self, parsed_contect, max_items=3):
        raise NotImplementedError()

class CommonNewsParser(AbstractNewsParser):
    ''' common implementation of the AbstractNewsParser '''

    def __init__(self):
        #Prohibit creating class instance
        if self.__class__ is AbstractNewsParser:
            raise TypeError('abstract class cannot be instanciated')

    def get_top_new(self):
        ''' template method which returns 3 latest news from every news website '''

        url = self.get_url()
        raw_content = self.get_raw_content(url)
        content = self.parse_content(raw_content)
        cropped = self.crop(content)

        return [{ field : item[field] for field in self.get_field_map().keys()} for item in cropped]

    def get_url(self):
        raise NotImplementedError()

    def get_raw_content(self, url):
        return urllib.request.urlopen(url).read()

    def parse_content(self, raw_content):
        raise NotImplementedError()

    def crop(self, parsed_contect, max_items=3):
        return parsed_contect[:max_items]
    def get_field_map(self):
        raise NotImplementedError()



class GoogleParser(CommonNewsParser):

    def get_url(self):
        ''' defining the url of google news '''

        #TODO move this to a configuration variable
        return 'https://news.google.com/news/?output=rss&gl=GB&ned=us&hl=en'

    def get_field_map(self):
        return dict(zip(['title', 'content', 'link', 'published', 'id'],
                   ['title', 'description', 'link', 'pubDate', 'guid']))

    def parse_content(self, raw_content):
        ''' implementation of content parser for google feed which is slightly different from bbc parser'''
        parsed_content = []

        dom = minidom.parseString(raw_content)
        for node in dom.getElementsByTagName('item'):

            parsed_item = {}
            for field, value in self.get_field_map().items():
                try:
                    if field == 'id':
                        node_value = node.getElementsByTagName(value)[0].childNodes[0].nodeValue
                        m = re.search(r'cluster=(.+)', node_value )
                        parsed_item[field] = None if m is None else m.group(1)
                    else:
                        parsed_item[field] = node.getElementsByTagName(value)[0].childNodes[0].nodeValue
                except IndexError:
                    parsed_item[field] = None
            parsed_content.append(parsed_item)

        return parsed_content

class BbcParser(CommonNewsParser):

    def get_url(self):
        ''' defining the url of bbc news '''

        #TODO move this to a configuration variable
        return 'http://feeds.bbci.co.uk/news/rss.xml?edition=uk'

    def get_field_map(self):
        return dict(zip(['title', 'content', 'link', 'published', 'id'],
                   ['title', 'description', 'link', 'pubDate', 'guid']))

    def parse_content(self, raw_content):
        ''' implementation of content parser for bbc feed'''

        parsed_content = []

        dom = minidom.parseString(raw_content)
        for node in dom.getElementsByTagName('item'):

            parsed_item = {}
            for field, value in self.get_field_map().items():
                try:
                    parsed_item[field] = node.getElementsByTagName(value)[0].childNodes[0].nodeValue
                except IndexError:
                    parsed_item[field] = None
            parsed_content.append(parsed_item)

        return parsed_content
